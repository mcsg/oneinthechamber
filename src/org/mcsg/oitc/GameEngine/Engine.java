package org.mcsg.oitc.GameEngine;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.mcsg.api.CoolStuff.SpectatorFactory;
import org.mcsg.oitc.OITC;
import org.mcsg.oitc.ConfigManager.Config;
import org.mcsg.oitc.ConfigManager.MapConfig;

public class Engine
{
	private static SpectatorFactory sf;
	private static VotingManager vManager;
	
	public static void Prep() {
		GameVariables.createMap();
		GameVariables.createGame();
		
		OITC.getPlugin().setConfig(new Config());
		OITC.getPlugin().setMapConfig(new MapConfig());
		
		sf = new SpectatorFactory(OITC.getPlugin());
		if (GameVariables.getVoting()) {
			vManager = new VotingManager(false);
		}
	}
	public static void addPlayer(Player p) {
		GameVariables.getGame().signup(p);
	}
	public static void playerLeave(Player p) {
		GameVariables.getGame().suttleLeave(p.getName());
	}
	
	public static void setSpectating(Player p) {
		if (sf.isSpectating(p)) { return; }
		sf.addSpectator(p);
		p.sendMessage(GameVariables.Prefix + ChatColor.LIGHT_PURPLE + "You are now spectating!");
	}
	public static boolean isSpectating(Player p) {
		return sf.isSpectating(p);
	}
	
	public static VotingManager getVotingManager() {
		return vManager;
	}
}
