package org.mcsg.oitc.GameEngine;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.mcsg.oitc.OPlayer;

public class GameVariables
{
	public static enum GameMode { ELIMINATION, KILLS, HARDCORE }
	
	private static Game CurrentGame;
	private static Map CurrentMap;
	
	private static List<OPlayer> Players = new ArrayList<OPlayer>();
	
	private static Integer maxPlayers = 24;
	private static Integer minPlayers = 2;
	private static Integer lobbyTime = 5;
	private static Integer lobbyShortTime = 15;
	private static Integer gameTime = 600;
	private static Integer lifes = 3;
	private static Integer lastSpawn = 0;
	private static boolean voting = true;
	public static GameMode gameMode = GameMode.ELIMINATION;
	protected static String Prefix = ChatColor.GREEN+"[OITC] ";
	
	public static Integer getMaxPlayers() {
		return maxPlayers;
	}
	public static void setMaxPlayers(Integer value) {
		maxPlayers = value;
	}
	public static Integer getMinPlayers() {
		return minPlayers;
	}
	public static void setMinPlayers(Integer value) {
		minPlayers = value;
	}
	public static Integer getLobbyTime() {
		return lobbyTime;
	}
	public static void setLobbyTime(Integer value) {
		lobbyTime = value;
	}
	public static Integer getLobbyShortTime() {
		return lobbyShortTime;
	}
	public static void setLobbyShortTime(Integer value) {
		lobbyShortTime = value;
	}
	public static Integer getGameTime() {
		return gameTime;
	}
	public static void setGameTime(Integer value) {
		gameTime = value;
	}
	public static Integer getLastSpawn() {
		return lastSpawn;
	}
	public static void setLastSpawn(Integer value) {
		lastSpawn = value;
	}
	public static boolean getVoting() {
		return voting;
	}
	public static void setVoting(boolean value) {
		voting = value;
	}
	public static Integer getLifes() {
		return lifes;
	}
	public static void setLifes(Integer value) {
		for(OPlayer o : getPlayers()) {
			o.setLifesLeft(value);
		}
		lifes = value;
	}
	public static GameMode getGameMode() {
		return gameMode;
	}
	public static String getPrefix() {
		return Prefix;
	}
	public static List<OPlayer> getPlayers() {
		return Players;
	}
	public static Map getMap() {
		return CurrentMap;
	}
	public static Map createMap() {
		CurrentMap = new Map();
		return CurrentMap;
	}
	public static Game getGame() {
		return CurrentGame;
	}
	public static Game createGame() {
		CurrentGame = new Game();
		return CurrentGame;
	}
	
	
	//Player methods
	public static OPlayer getPlayer(String name) {
		for(OPlayer o : getPlayers().toArray(new OPlayer[getPlayers().size()])) {
			if (o.getName().equalsIgnoreCase(name))
				return o;
		}
		return null;
	}
	public static OPlayer getPlayer(Player p) {
		for(OPlayer o : getPlayers().toArray(new OPlayer[getPlayers().size()])) {
			if (o.getName().equalsIgnoreCase(p.getName()))
				return o;
		}
		return null;
	}
	public static boolean containsPlayer(OPlayer player) {
		return getPlayer(player.getName()) != null;
	}
	public static boolean containsPlayer(Player player) {
		return getPlayer(player.getName()) != null;
	}
	public static void addPlayer(OPlayer g) {
		getPlayers().add(g);
	}
}
