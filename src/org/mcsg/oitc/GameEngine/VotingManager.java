package org.mcsg.oitc.GameEngine;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.mcsg.oitc.GameEngine.GameVariables.GameMode;

public class VotingManager
{
	//Static stuff
	public static enum VotingStage { OPEN, CLOSED; }
	public static String Prefix = ChatColor.DARK_PURPLE+"[VotingManager] "+ChatColor.GREEN;
	private static int _lastID = 1;
	public static int getID() {
		return _lastID++;
	}
	public static void setVariables(GameMode mode) {
		GameVariables.gameMode = mode;
		switch(mode) {
			case HARDCORE: {
				GameVariables.setLifes(1);
				GameVariables.setGameTime(200);
				break;
			}
			case ELIMINATION: {
				GameVariables.setLifes(4);
				GameVariables.setGameTime(500);
				break;
			}
			case KILLS: {
				GameVariables.setGameTime(250);
				break;
			}
		}
	}
	
	
	private boolean scoreboard = false;
	private VotingStage stage = VotingStage.OPEN;
	public List<String> Voted = new ArrayList<String>();
	public List<VoteOption> options = new ArrayList<VoteOption>();
	
	public VotingManager(boolean scoreboard)
	{
		this.scoreboard = scoreboard;
		addOption(new VoteOption(GameMode.ELIMINATION));
		addOption(new VoteOption(GameMode.HARDCORE));
		addOption(new VoteOption(GameMode.KILLS));
		
		setStage(VotingStage.OPEN);
	}
	
	public void finish() {
		close();
		announceWinner();
		setVariables(getWinner());
	}
	
	public void Vote(Integer ID, Player player) {
		if (Voted.contains(player.getName())) {
			player.sendMessage(Prefix + "You have already voted!");
			return;
		}
		else {
			VoteOption option = getOption(ID);
			if (option != null) {
				option.castVote(player);
				Voted.add(player.getName());
			}
			else {
				player.sendMessage(Prefix + "Invalid vote option!");
			}
		}
	}
	
	public void sendVotes() {
		for(Player p : Bukkit.getOnlinePlayers()) {
			//[VotingManager] ELIMINATION has 3 votes (/vote 1)
			//[VotingManager] KILLS has 5 votes (/vote 2)
			//[VotingManager] HARDCORE has 19 votes (/vote 3)
			for(VoteOption option : options) {
				p.sendMessage(Prefix + option.toString());
			}
		}
	}
	
	public void close() {
		setStage(VotingStage.CLOSED);
		GameVariables.getGame().sendMessageToGame(Prefix + "Voting is now closed! Collecting results...", false);
	}
	public void announceWinner() {
		GameVariables.getGame().sendMessageToGame(Prefix + "The winning gamemode is: "+ChatColor.RED+getWinner().toString(), false);
	}
	
	public GameMode getWinner() {
		int high = 0;
		GameMode winning = GameMode.ELIMINATION;
		for(VoteOption o : options) {
			if (o.getVotes() > high) {
				high = o.getVotes();
				winning = o.getMode();
			}
		}
		return winning;
	}
	
	public VoteOption getOption(Integer ID) {
		for(VoteOption o : options) {
			if (o.getID() == ID) {
				return o;
			}
		}
		return null;
	}
	public void addOption(VoteOption o) {
		if (!containsOption(o)) {
			options.add(o);
		}
	}
	public boolean containsOption(VoteOption o) {
		return getOption(o.getID()) != null;
	}
	public VotingStage getStage() {
		return stage;
	}
	public void setStage(VotingStage stage) {
		this.stage = stage;
	}
	public boolean isOpen() {
		return getStage() == VotingStage.OPEN;
	}
	public boolean getUseScoreboard() {
		return scoreboard;
	}
}
