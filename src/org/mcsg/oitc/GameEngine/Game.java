package org.mcsg.oitc.GameEngine;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.Sound;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.mcsg.oitc.OITC;
import org.mcsg.oitc.OPlayer;
import org.mcsg.oitc.GameTasks.GameTask;
import org.mcsg.oitc.GameTasks.LobbyTask;
import org.mcsg.oitc.GameTasks.TeleportTask;
import org.mcsg.oitc.ServerInteraction.InteractType;
import org.mcsg.oitc.ServerInteraction.ServerInteractor;

public class Game extends GameVariables
{
	public static enum GameStatus { WAITING, INGAME }
	
	//Variables
	private GameStatus status = GameStatus.WAITING;
	private LobbyTask lobbyTask;
	private GameTask gameTask;
	private Scoreboard scoreboard;
	
	public Game() { }
	
	//Methods
	public void signup(Player player) {
		if (Full()) {
			player.sendMessage(Prefix + ChatColor.GOLD + "The game is full! "+ChatColor.AQUA+":(");
			Leave(player);
		}
		else if (getStatus() == GameStatus.INGAME) {
			player.sendMessage(Prefix + ChatColor.GOLD + "The game is inprogress!");
			Leave(player);
		}
		else {
			hardJoin(player);
		}
	}
	
	public void start() {
		if (getStatus() == GameStatus.WAITING) {
			setStatus(GameStatus.INGAME);
			
			if (getPlayerCount() < getMinPlayers()) {
				sendMessageToGame(ChatColor.RED + "Uhoh! Not enough players to start the game!", true);
				scheduleCleanupOnIsle3(40);
			}
			else {
				new TeleportTask(true);
			}
		}
		else { System.out.println("WARNING: Tried to start game when GameStatus := "+status); }
	}
	public void playBall() {
		if (scoreboard == null) {
			scoreboard = new Scoreboard();
			scoreboard.updateScoreboard();
		}
		
		giveItems();
		gameTask = new GameTask(true);
		sendMessageToGame(ChatColor.GOLD + "This game is set to mode: "+ChatColor.RED+GameVariables.getGameMode(), true);
		sendMessageToGame(ChatColor.DARK_AQUA + "Play ball!", true);
	}
	public void outOfTime()
	{
		switch(getGameMode())
		{
			case HARDCORE:
			case ELIMINATION:
			{
				setAllSpectating();
				ChatColor random1 = getRandomColor();
				ChatColor random2 = getRandomColor();
				for(int i =0;i<10;i++){
					sendMessageToGame("", false);
				}
				sendMessageToGame(random1+""+ChatColor.BOLD+"^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=", false);
				sendMessageToGame(random2 + "         Timer is up and there is no last man standing! :(", false);
				sendMessageToGame(random1+""+ChatColor.BOLD+"^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=", false);
				break;
			}
			case KILLS:
			{
				setAllSpectating();
				OPlayer winner = getMostKills();
				if (winner != null) {
					ChatColor random1 = getRandomColor();
					ChatColor random2 = getRandomColor();
					ChatColor random3 = getRandomColor();
					for(int i =0;i<10;i++){
						sendMessageToGame("", false);
					}
					sendMessageToGame(random1+""+ChatColor.BOLD+"^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=", false);
					sendMessageToGame(random2 + "         "+winner.getName() + random3 + " has won the game!", false);
					sendMessageToGame(random1+""+ChatColor.BOLD+"^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=", false);
				}
				else {
					for(int i =0;i<10;i++){
						sendMessageToGame("", false);
					}
					sendMessageToGame(ChatColor.RED+""+ChatColor.BOLD+"^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=", false);
					sendMessageToGame(ChatColor.LIGHT_PURPLE + "         ERROR: UNABLE TO DETECT WINNER. ERROR", false);
					sendMessageToGame(ChatColor.RED+""+ChatColor.BOLD+"^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=", false);
				}
				break;
			}
		}
		scoreboard.updateScoreboard();
		scheduleCleanupOnIsle3(5*20);
	}
	public void setAllSpectating() {
		for(OPlayer p : getPlayers()) {
			Player player = p.getPlayer();
			clearInventory(player);
			Engine.setSpectating(player);
			player.setAllowFlight(true);
			player.setFlying(true);
		}
	}
	public OPlayer getMostKills() {
		Integer high = 0;
		OPlayer lastScorer = null;
		for(OPlayer o : getPlayers()) {
			if (o.getKills() > high) {
				lastScorer = o;
				high = o.getKills();
			}
		}
		return lastScorer;
	}
	public void giveItems() {
		for(OPlayer o : getPlayers()) {
			Player player = o.getPlayer();
			clearInventory(player);
			giveItems(player);
		}
	}
	@SuppressWarnings("deprecation")
	public void giveItems(Player player) {
		player.getInventory().setItem(0, new ItemStack(Material.WOOD_SWORD));
		player.getInventory().setItem(1, new ItemStack(Material.BOW));
		player.getInventory().setItem(8, new ItemStack(Material.ARROW));
		player.updateInventory();
	}
	

	private void hardJoin(Player player) {
		if (player.isFlying()) {
			player.setFlying(false);
		}
		if (player.getAllowFlight()) {
			player.setAllowFlight(false);
		}
		
		player.setHealth(player.getMaxHealth());
		player.setFoodLevel(20);
		
		clearInventory(player);
		OPlayer o = new OPlayer(player);
		addPlayer(o);
		
		sendMessageToGame(ChatColor.DARK_AQUA + player.getName() + ChatColor.GOLD + " has joined the game!", true);
		if (getStatus() == GameStatus.WAITING) {
			player.teleport(getMap().getLobby());
			
			if (getLobbyTask() == null) {
				setLobbyTask(new LobbyTask(Full(), true));
			}
			player.sendMessage(Prefix + ChatColor.YELLOW + "Welcome to "+ChatColor.GREEN+"OneInTheChamber!"+ChatColor.YELLOW + " Starting in: "+ChatColor.GREEN+getLobbyTask().timeLeft+ChatColor.YELLOW+" seconds!");
		}
		else {
			player.teleport(getMap().getNextSpawn()); 
		}
		
		ServerInteractor.notifyServer(InteractType.UPDATE);
	}
	
	public void processKill(Player killed, Player slayer) {
		if (slayer != null) {
			OPlayer os = getPlayer(slayer);
			if (os != null) {
				OPlayer ok = getPlayer(killed);
				if (ok != null) {
					os.addKill();
					ok.removeLife();
					
					Player p = os.getPlayer();
					ItemStack s = p.getInventory().getItem(8);
					if (s == null) {
						p.getInventory().setItem(8, new ItemStack(Material.ARROW, 1));
					}
					else {
						p.getInventory().setItem(8, new ItemStack(Material.ARROW, s.getAmount()+1));
					}
					switch(getGameMode())
					{
						case HARDCORE:
						case ELIMINATION:
						{
							if (getGameMode() == GameMode.HARDCORE) { slayer.setHealth(Math.min(slayer.getHealth()+1, 1)); }
							if (ok.getLifesLeft() <= 0) {
								sendMessageToGame(ChatColor.GOLD+ok.getName() + ChatColor.LIGHT_PURPLE + " has been eliminated from the round!", true);
								Engine.setSpectating(killed);
								killed.setAllowFlight(true);
								killed.setFlying(true);
							}
							else {
								sendMessageToGame(ChatColor.GOLD+ok.getName()+ChatColor.DARK_AQUA+" was killed by "+ChatColor.GOLD+os.getName(), true);
								killed.sendMessage(Prefix + ChatColor.AQUA + "You have "+ChatColor.RED+ok.getLifesLeft()+ChatColor.AQUA+" lives remaining!");
							}
							break;
						}
						case KILLS://Only need to add a kill to the player, which we do ^
						{
							sendMessageToGame(ChatColor.GOLD+ok.getName()+ChatColor.DARK_AQUA+" was killed by "+ChatColor.GOLD+os.getName(), true);
							break;
						}
					}
					scoreboard.updateScoreboard();
				}
			}
		}
		checkForWinner();
	}
	
	public void checkForWinner() {
		if (getGameMode() == GameMode.ELIMINATION || getGameMode() == GameMode.HARDCORE) {
			if (getAlivePlayers().size() == 1) {
				gameTask.Stop();
				OPlayer winner = getAlivePlayers().get(0);
				
				setAllSpectating();
				
				ChatColor random1 = getRandomColor();
				ChatColor random2 = getRandomColor();
				ChatColor random3 = getRandomColor();
				for(int i =0;i<10;i++){
					sendMessageToGame("", false);
				}
				sendMessageToGame(random1+""+ChatColor.BOLD+"^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=", false);
				sendMessageToGame(random2 + "         "+winner.getName() + random3 + " has won the game!", false);
				sendMessageToGame(random1+""+ChatColor.BOLD+"^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=^=", false);
			
				scheduleCleanupOnIsle3(7*20);
			}
		}
	}
	
	
	
	public void Leave(OPlayer o) {
		OITC.getLilypad().sendToHub(o.getPlayer());
		getPlayers().remove(o);
	}
	public void Leave(String name) {
		OPlayer o = getPlayer(name);
		if (o != null) {
			getPlayers().remove(o);
		}
		OITC.getLilypad().sendToHub(Bukkit.getPlayerExact(name));
	}
	public void Leave(Player p) {
		Leave(p.getName());
	}
	public void suttleLeave(String name) {
		OPlayer o = getPlayer(name);
		if (o != null) {
			getPlayers().remove(o);
		}
	}
	private void cleanup() {
		ServerInteractor.notifyServer(InteractType.SHUTDOWN);
		OITC.setAcceptingPlayers(false);
		
		for(Player p : Bukkit.getOnlinePlayers()) {
			Leave(p);
		}
		
		Bukkit.getScheduler().scheduleSyncDelayedTask(OITC.getPlugin(), new Runnable()
		{
			public void run() {
				Bukkit.getServer().shutdown();
			}
		}, 40);
	}
	public void scheduleCleanupOnIsle3(final int ticks) {
		ServerInteractor.notifyServer(InteractType.SHUTDOWN);
		OITC.setAcceptingPlayers(false);
		Bukkit.getScheduler().scheduleSyncDelayedTask(OITC.getPlugin(), new Runnable()
		{
			public void run() {
				cleanup();
			}
		}, ticks);
	}
	
	public static ChatColor getRandomColor()
    {
    	Random r = new Random();
    	int pick = r.nextInt(16);
    	switch(pick)
    	{
	    	case 0: return ChatColor.AQUA;
	    	case 1: return ChatColor.BLACK;
	    	case 2: return ChatColor.BLUE;
	    	case 3: return ChatColor.DARK_AQUA;
	    	case 4: return ChatColor.DARK_BLUE;
	    	case 5: return ChatColor.DARK_GRAY;
	    	case 6: return ChatColor.DARK_GREEN;
	    	case 7: return ChatColor.DARK_PURPLE;
	    	case 8: return ChatColor.DARK_RED;
	    	case 9: return ChatColor.GOLD;
	    	case 10: return ChatColor.GRAY;
	    	case 11: return ChatColor.GREEN;
	    	case 12: return ChatColor.LIGHT_PURPLE;
	    	case 13: return ChatColor.RED;
	    	case 14: return ChatColor.WHITE;
	    	case 15: return ChatColor.YELLOW;
    	}
    	return ChatColor.GREEN;
    }
	
	public void sendMessageToGame(String msg, boolean prefix) {
		String tosend = msg;
		if (prefix) {
			tosend = getPrefix()+msg;
		}
		for(OPlayer o : getPlayers()) {
			o.getPlayer().sendMessage(tosend);
		}
	}
	public void sendSound(Sound sound) {
		for(OPlayer o : getPlayers()) {
			//Store player, 0.0000001ms faster
			Player p = o.getPlayer();
			p.playSound(p.getLocation(), sound, 100, 1);
		}
	}
	public void setAllEXP(Integer level) {
		for (OPlayer o : getPlayers()) {
			o.getPlayer().setLevel(level);
		}
	}
    @SuppressWarnings("deprecation")
    public void clearInventory(Player player) {
        player.getInventory().setArmorContents(new ItemStack[4]);
        player.getInventory().setContents(new ItemStack[]{});
       
        //Make sure, that shit, is fucking clear
        player.getInventory().setHelmet(new ItemStack(Material.AIR));
        player.getInventory().setLeggings(new ItemStack(Material.AIR));
        player.getInventory().setBoots(new ItemStack(Material.AIR));
        player.getInventory().setChestplate(new ItemStack(Material.AIR));
        player.updateInventory();
    }
	
	// Get & Set accessors
	public GameStatus getStatus() {
		return status;
	}
	private void setStatus(GameStatus status) {
		this.status = status;
		ServerInteractor.notifyServer(InteractType.UPDATE);
	}
	public LobbyTask getLobbyTask() {
		return lobbyTask;
	}
	public void setLobbyTask(LobbyTask task) {
		lobbyTask = task;
	}
	public Integer getPlayerCount() {
		return getPlayers().size();
	}
	public boolean Full() { 
		return getPlayerCount() >= getMaxPlayers();
	}

	public List<OPlayer> getAlivePlayers() {
		List<OPlayer> players = new ArrayList<OPlayer>();
		for(OPlayer o : getPlayers()) {
			if (o.getLifesLeft() > 0) {
				players.add(o);
			}
		}
		return players;
	}
	public List<OPlayer> getDeadPlayers() {
		List<OPlayer> players = new ArrayList<OPlayer>();
		for(OPlayer o : getPlayers()) {
			if (o.getLifesLeft() <= 0) {
				players.add(o);
			}
		}
		return players;
	}
}
