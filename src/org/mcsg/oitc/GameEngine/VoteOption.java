package org.mcsg.oitc.GameEngine;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.mcsg.oitc.GameEngine.GameVariables.GameMode;

public class VoteOption
{
	private GameMode mode = null;
	private Integer votes = 0;
	private Integer ID = 0;
	public List<String> voted = new ArrayList<String>();
	
	public VoteOption(GameMode mode) {
		this.mode = mode;
		ID = VotingManager.getID();
	}
	public void castVote(Player p) {
		voted.add(p.getName());
		votes++;
		
		GameVariables.getGame().sendMessageToGame(VotingManager.Prefix + ""+ChatColor.YELLOW+p.getName()+ChatColor.GREEN + " has voted for "+ChatColor.AQUA+this.mode.toString(), false);
	}
	public Integer getVotes() {
		return votes;
	}
	public Integer getID() {
		return ID;
	}
	public GameMode getMode() {
		return mode;
	}
	
	@Override
	public String toString() {
		return new String(ChatColor.DARK_AQUA + mode.toString() + ChatColor.GOLD + " has "+ChatColor.AQUA+getVotes()+ChatColor.GOLD+" votes! (/vote "+ChatColor.AQUA+getID()+ChatColor.GOLD+")");
	}
}
