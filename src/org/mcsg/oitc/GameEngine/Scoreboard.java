package org.mcsg.oitc.GameEngine;

import java.util.Comparator;
import java.util.Iterator;
import java.util.Random;
import java.util.SortedSet;
import java.util.TreeSet;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Score;
import org.bukkit.scoreboard.ScoreboardManager;
import org.mcsg.oitc.OITC;
import org.mcsg.oitc.OPlayer;

public class Scoreboard
{
	ScoreboardManager manager;
	org.bukkit.scoreboard.Scoreboard scoreboard;
	Objective objective;
	
	public Scoreboard() {
		manager = OITC.getPlugin().getServer().getScoreboardManager();
	}
	
	public void showsScoreboards() {
		renewScoreboard();
		updateScoreboard();
	}
	public void renewScoreboard() {
		scoreboard = manager.getNewScoreboard();
		switch(GameVariables.getGameMode())
		{
			case ELIMINATION:
			case HARDCORE:
			{
				objective = scoreboard.registerNewObjective("PlayerLives", "PlayerLives"+new Random().nextInt());
				objective.setDisplayName(ChatColor.GREEN + "Player Lives");
				break;
			}
			case KILLS: {
				objective = scoreboard.registerNewObjective("Kills", "Kills"+new Random().nextInt());
				objective.setDisplayName(ChatColor.GREEN + "Top Killers");
				break;
			}
		}
		objective.setDisplaySlot(DisplaySlot.SIDEBAR);
	}
	public void sendScoreboard() {
		for (OPlayer o : GameVariables.getPlayers()) {
			o.getPlayer().setScoreboard(scoreboard);
		}
	}
	public void updateScoreboard() {
		renewScoreboard();
		
		Score[] Scores = new Score[9];
		switch(GameVariables.getGameMode()) {
			case ELIMINATION:
			case HARDCORE:
			{
				if (GameVariables.getGame().getAlivePlayers().size() < 9) {
					Scores = new Score[GameVariables.getGame().getAlivePlayers().size()];
				}
				
				int counter = 0;
				for(Iterator<OPlayer> i = sortByLifes().iterator(); i.hasNext();) {
					OPlayer player = i.next();
					String name = player.getName();
					if (name.length() > 14) {
						name = name.substring(0, 14);
					}
					Integer lifes = player.getLifesLeft();
					if (counter < Scores.length) {
						if (player.getName().equalsIgnoreCase("double0negative") || name.equalsIgnoreCase("dread9nought")) {
							Scores[counter] = objective.getScore(Bukkit.getOfflinePlayer(ChatColor.GREEN + name));
						}
						else if (player.getName().equalsIgnoreCase("chaskyt")) {
							Scores[counter] = objective.getScore(Bukkit.getOfflinePlayer(ChatColor.AQUA + name));
						}
						else {
							Scores[counter] = objective.getScore(Bukkit.getOfflinePlayer(ChatColor.LIGHT_PURPLE + name));
						}
						Scores[counter].setScore(lifes);
						counter++;
					}
					else {
						break;
					}
					
				}
				
				
				break;
			}
			case KILLS: {
				int counter = 0;
				for(Iterator<OPlayer> i = sortByKills().iterator(); i.hasNext();) {
					OPlayer player = i.next();
					String name = player.getName();
					if (name.length() > 14) {
						name = name.substring(0, 14);
					}
					Integer kills = player.getKills();
					if (counter < Scores.length) {
						if (player.getName().equalsIgnoreCase("double0negative") || name.equalsIgnoreCase("dread9nought")) {
							Scores[counter] = objective.getScore(Bukkit.getOfflinePlayer(ChatColor.GREEN + name));
						}
						else if (player.getName().equalsIgnoreCase("chaskyt")) {
							Scores[counter] = objective.getScore(Bukkit.getOfflinePlayer(ChatColor.AQUA + name));
						}
						else {
							Scores[counter] = objective.getScore(Bukkit.getOfflinePlayer(ChatColor.LIGHT_PURPLE + name));
						}
						Scores[counter].setScore(kills);
						counter++;
					}
					else {
						break;
					}
					
				}
				break;
			}
		}
		sendScoreboard();
	}
	public static SortedSet<OPlayer> sortByKills() {
		SortedSet<OPlayer> players = new TreeSet<OPlayer>(
				new Comparator<OPlayer>() {
					@Override
					public int compare(OPlayer e1, OPlayer e2) {
						int res = e1.getKills().compareTo(e2.getKills());
						return res != 0 ? res : 1;
					}
				});
		players.addAll(GameVariables.getPlayers());
		return players;
	}
	public static SortedSet<OPlayer> sortByLifes() {
		SortedSet<OPlayer> players = new TreeSet<OPlayer>(
				new Comparator<OPlayer>() {
					@Override
					public int compare(OPlayer e1, OPlayer e2) {
						int res = e1.getLifesLeft().compareTo(e2.getLifesLeft());
						return res != 0 ? res : 1;
					}
				});
		players.addAll(GameVariables.getGame().getAlivePlayers());
		return players;
	}
}
