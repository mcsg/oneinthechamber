package org.mcsg.oitc;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.mcsg.oitc.GameEngine.GameVariables;

public class OPlayer
{
	private Integer lifesLeft = GameVariables.getLifes();
	private Integer kills = 0;
	private String name;
	
	public OPlayer(Player p) { name = p.getName(); }
	public OPlayer(String pname) { name = pname; }
	
	public Integer getLifesLeft() {
		return lifesLeft;
	}
	public void setLifesLeft(Integer lifes) { 
		lifesLeft = lifes;
	}
	public Integer removeLife() {
		return lifesLeft--;
	}
	public Integer addLife() {
		return lifesLeft++;
	
	}
	public Integer getKills() {
		return kills;
	}
	public void setKills(Integer kills) {
		this.kills = kills;
	}
	public Integer removeKill() {
		return kills--;
	}
	public Integer addKill() {
		return kills++;
	}

	public String getName() {
		return name;
	}
	public Player getPlayer() {
		return Bukkit.getPlayerExact(name);
	}
}
