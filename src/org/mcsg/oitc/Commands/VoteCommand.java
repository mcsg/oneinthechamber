package org.mcsg.oitc.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mcsg.oitc.GameEngine.Engine;
import org.mcsg.oitc.GameEngine.VotingManager;

public class VoteCommand implements CommandExecutor
{
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (Engine.getVotingManager() != null) {
            	if (args.length >= 1)
            	{
            		try
            		{
            			if (Engine.getVotingManager().isOpen()) {
            				Engine.getVotingManager().Vote(Integer.parseInt(args[0]), player);
            			}
            			else {
            				player.sendMessage(VotingManager.Prefix + "Voting is no longer open!");
            			}
            				
            		}
            		catch(Exception e) {
            			player.sendMessage(VotingManager.Prefix + "You should probably use a number huh?");
            		}
            	}
            	else {
            		player.sendMessage(VotingManager.Prefix + "You must specify what to vote for!");
            	}
            }
            else {
            	player.sendMessage(VotingManager.Prefix + ChatColor.RED + "Voting is not enabled!");
            }
        }
        return true;
	}
}
