package org.mcsg.oitc.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mcsg.oitc.GameEngine.GameVariables;

public class LeaveCommand implements CommandExecutor
{
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            player.sendMessage(GameVariables.getPrefix() + "You are now leaving OITC!");
            GameVariables.getGame().Leave(player);
        }
        return true;
	}
}
