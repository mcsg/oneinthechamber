package org.mcsg.oitc.Commands;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mcsg.oitc.GameEngine.GameVariables;
import org.mcsg.oitc.GameEngine.Game.GameStatus;

public class ForcestartCommand implements CommandExecutor
{
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (!player.hasPermission("oitc.forcestart")) { return true; }
            
            if (GameVariables.getGame().getStatus() == GameStatus.WAITING) {
            	GameVariables.getGame().getLobbyTask().Stop();
            	GameVariables.getGame().start();
            }
        }
        return true;
	}
}
