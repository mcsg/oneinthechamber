package org.mcsg.oitc.Commands;

import org.bukkit.ChatColor;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.mcsg.oitc.GameEngine.GameVariables;
import org.mcsg.oitc.GameEngine.VotingManager;
import org.mcsg.oitc.GameEngine.Game.GameStatus;

public class ModeCommand implements CommandExecutor
{
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
        if (sender instanceof Player) {
            Player player = (Player) sender;
            if (args.length == 0) { showMode(player); }
            else {
            	if (player.isOp()) {
            		if (GameVariables.getGame().getStatus() == GameStatus.WAITING) {
            			try {
            				GameVariables.GameMode targetMode = GameVariables.GameMode.valueOf(args[0].toUpperCase());
            				VotingManager.setVariables(targetMode);
            				GameVariables.getGame().sendMessageToGame(ChatColor.YELLOW+"The game mode has been changed to: "+ChatColor.RED+targetMode.toString(), true);
            			}
	            		catch(Exception e) {
	            			player.sendMessage(GameVariables.getPrefix() + ChatColor.RED + "ERROR!");
	            		}
            		}
            		else {
            			player.sendMessage(GameVariables.getPrefix() + ChatColor.RED + "Cannot change mode when game has already started! :(");
            		}
            	}
            	else {
            		showMode(player);
            	}
            }
        }
        return true;
	}
	
	public void showMode(Player player) {
		player.sendMessage(GameVariables.getPrefix() + "The current game is set to mode: "+ChatColor.RED+GameVariables.getGameMode());
	}
}
