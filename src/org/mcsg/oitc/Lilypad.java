package org.mcsg.oitc;

import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.mcsg.oitc.ServerInteraction.InteractType;
import org.mcsg.oitc.ServerInteraction.ServerInteractor;

import lilypad.client.connect.api.Connect;
import lilypad.client.connect.api.event.EventListener;
import lilypad.client.connect.api.event.MessageEvent;
import lilypad.client.connect.api.request.impl.RedirectRequest;
import lilypad.client.connect.api.result.FutureResultListener;
import lilypad.client.connect.api.result.StatusCode;
import lilypad.client.connect.api.result.impl.RedirectResult;


public class Lilypad 
{
	Connect connect;
	private static String hubServer = "hub";
	
	String kickMessage = ChatColor.LIGHT_PURPLE +"\"Beam me up, Scotty\"\n" +
			ChatColor.YELLOW+""+ChatColor.ITALIC+"Uh, well.. this is embarrassing\n\n"+
			ChatColor.AQUA+                    "Logic would dictate Scotty's transporters may need re-calibrating\n\n\n\n"+
			ChatColor.GREEN+""+ChatColor.ITALIC+"^click me for explosions!";
	
	public Lilypad()
	{
		connect = getConnect();
		connect.registerEvents(this);
	}
	public void sendToHub(final Player player)
	{
		if (!player.isOnline()) { return; }
	    try
	    {
	    	if (this.connect == null || this.connect.isClosed()) { connect = getConnect(); }
		    if (this.connect.isConnected()) {
		    	this.connect.request(new RedirectRequest(getHubServer(), player.getName())).registerListener(new FutureResultListener<RedirectResult>() {	
		    		public void onResult(RedirectResult redirectResult) {
			    		if (redirectResult.getStatusCode() != StatusCode.SUCCESS) {
			    			player.sendMessage(ChatColor.RED + "Connection error");
			    			
			    			player.kickPlayer(kickMessage);
			    		}
			    		else { return; }
		    		}
		    	});
		    }
		    else {
		    	player.sendMessage(ChatColor.RED+"[ERROR] Connection down");
		    }
	    }
		catch (Exception exception) {
		    player.sendMessage(ChatColor.RED + "Connection exception");
		    player.kickPlayer(kickMessage);
		}
	}
	
	
	
	
	@EventListener
	public void onMessage(MessageEvent e) {
		String channel = e.getChannel();
		if (channel.equalsIgnoreCase("oitcsignserver")) {
			byte[] packet = e.getMessage();
			if (packet[0] == 0x02)//ping packet
			{
				ServerInteractor.notifyServer(InteractType.UPDATE);
			}
		}
	}
	
	// Static stuffz
	
	public static Connect getConnect() {
		return OITC.getPlugin().getServer().getServicesManager().getRegistration(Connect.class).getProvider();
	}
	
	public static String getHubServer() {
		return hubServer;
	}
	public static void setHubServer(String str) {
		hubServer = str;
	}
}
