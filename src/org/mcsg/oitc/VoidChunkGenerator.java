package org.mcsg.oitc;

import java.util.Random;

import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.block.Biome;
import org.bukkit.generator.ChunkGenerator;


//Special thanks to Courier
public class VoidChunkGenerator extends ChunkGenerator
{
	@Override
	public Location getFixedSpawnLocation(World world, Random random) {
        return new Location(world, 0, 70, 0);
    }
    @Override
    public byte[][] generateBlockSections(World world, Random random, int x, int z, BiomeGrid biomes)
    {
    	for(int bx = 0; bx < 16; ++bx)
    	    for(int bz = 0; bz < 16; ++bz)
    	        biomes.setBiome(bx, bz, Biome.HELL);
        return new byte[16][];
    }
}