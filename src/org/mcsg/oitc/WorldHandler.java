package org.mcsg.oitc;

import org.bukkit.ChatColor;
import org.mcsg.api.fileserver.FileServer;

public class WorldHandler
{
	private static StaticWorldSystem gameWorldSystem;
	private static StaticWorldSystem lobbyWorldSystem;
	
	private static String gameWorldName = "GameWorld";
	private static String lobbyWorldName = "LobbyWorld";
	
	public static void initWorlds(boolean download) {
		gameWorldSystem = getStaticWorldSystem();
		lobbyWorldSystem = getStaticWorldSystem();
		
		sendMessage("Initialized worlds - Downloading: "+download);
		
		if (download) { downloadWorlds(); }
	}
	public static void downloadWorlds() {
		if (gameWorldSystem == null && lobbyWorldSystem == null) { initWorlds(false); }
		
		//gameWorldName = StaticWorldSystem.DownloadRandomWorld();
		String AbsoPath = OITC.getPlugin().getServer().getWorldContainer().getAbsolutePath().toString();
		//Remove weird dot at the end
		String path = AbsoPath.substring(0, AbsoPath.length()-1);
		
		FileServer.getRandomFolder("map", "OITC/", path);
	}
	public static void loadWorlds() {
		gameWorldSystem.LoadWorld(gameWorldName);
		lobbyWorldSystem.LoadWorld(lobbyWorldName);
		
		sendMessage("Loaded worlds. GameWorldName: "+ChatColor.AQUA+getGameWorldName());
	}
	public static void deleteWorlds() {
		if (gameWorldSystem != null) {
			gameWorldSystem.DeleteWorld();
			sendMessage("Deleted game world("+ChatColor.GOLD+getGameWorldName()+ChatColor.LIGHT_PURPLE+")");
		}
		if (lobbyWorldSystem != null) {
			lobbyWorldSystem.DeleteWorld();
			sendMessage("Deleted lobby world("+ChatColor.GOLD+getLobbyWorldName()+ChatColor.LIGHT_PURPLE+")");
		}
	}
	
	private static void sendMessage(String msg) {
		OITC.getConsoleCommandSender().sendMessage(ChatColor.YELLOW+"[WorldHandler] "+ChatColor.LIGHT_PURPLE+msg);
	}
	public static StaticWorldSystem getGameWorld() {
		return gameWorldSystem;
	}
	public static StaticWorldSystem getLobbyWorld() {
		return lobbyWorldSystem;
	}
	public static String getGameWorldName() {
		return gameWorldName;
	}
	public static String getLobbyWorldName() {
		return lobbyWorldName;
	}
	public static StaticWorldSystem getStaticWorldSystem() {
		return new StaticWorldSystem();
	}
}
