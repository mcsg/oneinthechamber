package org.mcsg.oitc.GameTasks;

import java.util.Arrays;

import org.bukkit.entity.Player;
import org.mcsg.oitc.OITC;
import org.mcsg.oitc.OPlayer;
import org.mcsg.oitc.GameEngine.GameVariables;
import org.mcsg.oitc.GameEngine.GameVariables.GameMode;

public class TeleportTask
{
	int TaskID;
	int lastindex;
	OPlayer[] players;
	
	public TeleportTask(boolean start) {
		lastindex = 0;
		players = new OPlayer[GameVariables.getPlayers().size()];
		System.arraycopy(GameVariables.getPlayers().toArray(new OPlayer[GameVariables.getPlayers().size()]), 0, players, 0, GameVariables.getPlayers().size());
		
		if (start) {
			Start();
		}
	}
	
	public void Start()
	{
		TaskID = OITC.getPlugin().getServer().getScheduler().scheduleSyncRepeatingTask(OITC.getPlugin(), new Runnable()
		{
			public void run()
			{
				if (lastindex == players.length) {
					GameVariables.getGame().playBall();
					Stop();
					return;
				}
				
				Player p = players[lastindex].getPlayer();
				if (p != null) {
					p.teleport(GameVariables.getMap().getNextSpawn());
					if (GameVariables.getGameMode() == GameMode.HARDCORE) {
						p.setHealth(1);
					}
					else {
						p.setHealth(20);
						p.setFoodLevel(20);
					}
				}
				lastindex++;
			}
		}, 0L, 2L);
	}
	public void Stop()
	{
		OITC.getPlugin().getServer().getScheduler().cancelTask(TaskID);
		empty();
	}
	private void empty() {
		Arrays.fill(players, null);
		players = new OPlayer[0];
	}
}
