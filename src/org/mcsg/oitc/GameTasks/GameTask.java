package org.mcsg.oitc.GameTasks;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.mcsg.oitc.OITC;
import org.mcsg.oitc.OPlayer;
import org.mcsg.oitc.GameEngine.GameVariables;

public class GameTask
{
	int TaskID;
	Integer timeLeft;
	public GameTask(boolean start) {
		timeLeft = GameVariables.getGameTime();
		if (start) {
			Start();
		}
	}
	
	public void Start() {
		TaskID = OITC.getPlugin().getServer().getScheduler().scheduleSyncRepeatingTask(OITC.getPlugin(), new Runnable()
		{
			public void run()
			{
				GameVariables.getGame().setAllEXP(timeLeft);
				if (timeLeft == 0) {
					GameVariables.getGame().outOfTime();
					Stop();
					return;
				}
				
				if (GameVariables.getGame().getPlayerCount() < 2) {
					GameVariables.getGame().sendMessageToGame(ChatColor.RED + "Oh no! All players left! :(", true);
					GameVariables.getGame().scheduleCleanupOnIsle3(20);
				}
				
				if (timeLeft % 60 == 0) {
					switch(GameVariables.getGameMode()) {
						case HARDCORE:
						case ELIMINATION:
						{
							GameVariables.getGame().sendMessageToGame(ChatColor.DARK_AQUA + "There are "+ChatColor.GOLD+GameVariables.getGame().getAlivePlayers().size()+ChatColor.DARK_AQUA+" players still alive!", true);
							break;
						}
						case KILLS:
						{
							OPlayer leader = GameVariables.getGame().getMostKills();
							if (leader != null) {
								GameVariables.getGame().sendMessageToGame(ChatColor.RED + leader.getName() + ChatColor.GREEN + " is winning!", true);
							}
							break;
						}
					}
					
				}
				if (timeLeft % 30 == 0) {
					GameVariables.getGame().sendMessageToGame(ChatColor.GOLD+"The game will end in "+ChatColor.DARK_AQUA+timeLeft+ChatColor.GOLD+" seconds", true);
				}
				timeLeft--;
			}
		}, 0L, 20L);
	}
	public void Stop() {
		Bukkit.getScheduler().cancelTask(TaskID);
	}
}
