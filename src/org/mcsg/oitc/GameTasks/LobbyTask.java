package org.mcsg.oitc.GameTasks;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.mcsg.oitc.OITC;
import org.mcsg.oitc.OPlayer;
import org.mcsg.oitc.GameEngine.Engine;
import org.mcsg.oitc.GameEngine.GameVariables;

public class LobbyTask
{
	public Integer timeLeft;
	public Integer votingCounter = 15;
	private Integer taskID;
	private Integer lastIndex = 0;
	private List<String> messages;
	
	public LobbyTask(boolean speeeeeedddyyybrooombroooom, boolean start) {
		if (speeeeeedddyyybrooombroooom) {
			timeLeft = GameVariables.getLobbyShortTime();
		}
		else {
			timeLeft = GameVariables.getLobbyTime();
		}
		if (start) {
			Start();
		}
	}
	
	private void populateMessages() {
		messages = new ArrayList<String>();
		messages.add(ChatColor.YELLOW + "The following map will be played: "+ChatColor.AQUA+GameVariables.getMap().getName());
		messages.add(ChatColor.YELLOW + "There are: "+ChatColor.AQUA+GameVariables.getGame().getPlayerCount() + ChatColor.YELLOW + " players waiting to play");
		messages.add(ChatColor.YELLOW + "The game will start in: "+ChatColor.AQUA+timeLeft+ChatColor.YELLOW+" seconds");
	}
	
	public void Start() {
		taskID = OITC.getPlugin().getServer().getScheduler().scheduleSyncRepeatingTask(OITC.getPlugin(), new Runnable()
		{
			public void run()
			{
				GameVariables.getGame().setAllEXP(timeLeft);
				
				if (timeLeft <= 0) {
					GameVariables.getGame().start();
					Stop();
					return;
				}
				
				if (timeLeft % 10 == 0 || timeLeft <= 5) {
					GameVariables.getGame().sendMessageToGame(getMessage(), true);
				}
				if (timeLeft == 10) { 
					if (Engine.getVotingManager() != null) {
						Engine.getVotingManager().finish();
					}
				}
				if (votingCounter == 0 && timeLeft >= 15) {
					votingCounter = 15;
					if (Engine.getVotingManager() != null) {
						if (Engine.getVotingManager().isOpen()) {
							Engine.getVotingManager().sendVotes();
						}
					}
				}
				for(OPlayer o : GameVariables.getPlayers().toArray(new OPlayer[GameVariables.getPlayers().size()]))
				{
					Player player = o.getPlayer();
					if (player != null) {
						if (!player.getWorld().getName().equalsIgnoreCase(GameVariables.getMap().getLobby().getWorld().getName()))
							player.teleport(GameVariables.getMap().getLobby());
					}
				}
				timeLeft--;
				votingCounter--;
			}
		}, 0L, 20L);
	}
	public void Stop() {
		Bukkit.getScheduler().cancelTask(taskID);
	}
	
	public String getMessage() {
		populateMessages();
		
		if (lastIndex == messages.size()) { lastIndex = 0; }
		
		String rtn = messages.get(2);
		if (timeLeft > 5) {
			rtn = messages.get(lastIndex);
		}
		lastIndex++;
	
		return rtn;
	}
}
