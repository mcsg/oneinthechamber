package org.mcsg.oitc;

import org.bukkit.Bukkit;
import org.bukkit.Color;
import org.bukkit.Effect;
import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Player;
import org.bukkit.entity.Projectile;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockBreakEvent;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.event.entity.CreatureSpawnEvent;
import org.bukkit.event.entity.EntityDamageByEntityEvent;
import org.bukkit.event.entity.EntityDamageEvent;
import org.bukkit.event.entity.EntityRegainHealthEvent;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.entity.EntityDamageEvent.DamageCause;
import org.bukkit.event.entity.PlayerDeathEvent;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryType.SlotType;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.event.player.PlayerPickupItemEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.event.player.PlayerRespawnEvent;
import org.mcsg.api.events.FileTransferCompleteEvent;
import org.mcsg.api.lib.util.FireworkEffectPlayer;
import org.mcsg.oitc.GameEngine.Engine;
import org.mcsg.oitc.GameEngine.GameVariables;
import org.mcsg.oitc.GameEngine.Game.GameStatus;
import org.mcsg.oitc.GameEngine.GameVariables.GameMode;

public class Events implements Listener
{
	public static void setup() {
		OITC.getPlugin().getServer().getPluginManager().registerEvents(new Events(), OITC.getPlugin());
	}
	
	@EventHandler
	public void onComplete(FileTransferCompleteEvent e) {
		System.out.println("Complete FTransfer from: "+e.getName());
		if (e.getName().equalsIgnoreCase("map")) {
			OITC.getPlugin().finishEnable();
		}
	}
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		e.setJoinMessage(null);
		
		Player p = e.getPlayer();
		if (!OITC.getAcceptingPlayers()) {
			OITC.getLilypad().sendToHub(p);
		}
		else {
			if (p.isDead()) {
				OITC.instaRespawn(p);
			}
			Engine.addPlayer(e.getPlayer());
		}
	}
	@EventHandler
	public void onRegen(EntityRegainHealthEvent e) {
		if (e.getEntity() instanceof Player) {
			if (GameVariables.getGameMode() == GameMode.HARDCORE) {
				e.setCancelled(true);
			}
		}
	}
	@EventHandler
	public void onRage(PlayerQuitEvent e) {
		e.setQuitMessage(null);
		Engine.playerLeave(e.getPlayer());
	}
	
	@EventHandler
	public void onDamage(EntityDamageEvent e) {
		if (GameVariables.getGame() != null) {
			if (GameVariables.getGame().getStatus() != GameStatus.INGAME) {
				e.setCancelled(true);
			}
			else if (e.getCause() != DamageCause.PROJECTILE && e.getCause() != DamageCause.ENTITY_ATTACK) {
				e.setCancelled(true);
			}
		}
	}
	
	@EventHandler
	public void onAttacked(EntityDamageByEntityEvent e) {
		if (GameVariables.getGame().getStatus() != GameStatus.INGAME) {
			e.setCancelled(true);
			return;
		}
		
		if (e.getEntity() instanceof Player)
		{
			Player hit = (Player)e.getEntity();
			if (Engine.isSpectating(hit)) {
				e.setCancelled(true);
				return;
			}
			Player attacker = null;
			if (e.getDamager() instanceof Player) {
				attacker = (Player)e.getDamager();
				if (Engine.isSpectating(attacker)) {
					e.setCancelled(true);
					return;
				}
			}
			else if (e.getDamager() instanceof Projectile) {
				attacker = (Player)((Projectile)e.getDamager()).getShooter();
			}
			
			if (hit.getName().equalsIgnoreCase(attacker.getName())) { e.setCancelled(true); return; }
			
			if (e.getCause() == DamageCause.PROJECTILE) {
				
				Location loc = hit.getLocation();
				FireworkEffectPlayer fep = new FireworkEffectPlayer();
				FireworkEffect effect = FireworkEffect.builder().trail(true).withColor(Color.AQUA).withFade(Color.TEAL).flicker(false).with(FireworkEffect.Type.BURST).build();
				try { fep.playFirework(loc.getWorld(), loc, effect); }  catch (Exception x) { }
				
				
				hit.damage(5000);
				
				GameVariables.getGame().processKill(hit, attacker);
				
				playerBlood(e.getEntity());
				
				e.setCancelled(true);
				return;
			}
			playerBlood(e.getEntity());
		}
	}
	public void playerBlood(Entity e) {
		Location loc = e.getLocation().clone();
		for (int i =0;i<1;i++) {
			e.getWorld().playEffect(loc, Effect.STEP_SOUND, 152);
		}
		loc.setY(loc.getY()+1);
		for (int i =0;i<1;i++) {
			e.getWorld().playEffect(loc, Effect.STEP_SOUND, 152);
		}
	}
	@EventHandler
	public void onDeath(final PlayerDeathEvent e) {
		if (e.getDeathMessage().toLowerCase().contains("died")) {
			
		}
		else if (GameVariables.getGame().getStatus() == GameStatus.INGAME) {
    		GameVariables.getGame().processKill(e.getEntity(), e.getEntity().getKiller());
    	}
		e.setDeathMessage(null);
    	e.setDroppedExp(0);
    	e.setKeepLevel(true);
    	e.getDrops().clear();
    	
    	Bukkit.getScheduler().scheduleSyncDelayedTask(OITC.getPlugin(), new Runnable()
    	{
    		final Player player = e.getEntity();
    		public void run()
    		{
    			if (player.isDead()) {
					OITC.instaRespawn(player);
    			}
    		}
    	}, 20L);
	}
	@EventHandler
	public void onRevive(PlayerRespawnEvent e) {
		if (GameVariables.getGame().getStatus() == GameStatus.WAITING) {
			e.setRespawnLocation(GameVariables.getMap().getLobby());
		}
		else {
			e.setRespawnLocation(GameVariables.getMap().getNextSpawn());
			
			if (!Engine.isSpectating(e.getPlayer())) {
				GameVariables.getGame().giveItems(e.getPlayer());
			}
		}
	}
	@EventHandler
    public void onSpawn(CreatureSpawnEvent e)
    {
    	e.setCancelled(true);
    }
    @EventHandler
    public void onDrop(PlayerDropItemEvent e)
    {
    	e.setCancelled(true);
    }
    @EventHandler
    public void onDrop(PlayerPickupItemEvent e)
    {
    	e.setCancelled(true);
    }
    @EventHandler
    public void onClick(InventoryClickEvent e)
    {
        if (e.getWhoClicked() instanceof Player) {
            if (e.getCurrentItem() != null) {
                	if (e.getSlotType() == SlotType.ARMOR)
                		e.setCancelled(true);
            }
        }
    }
    @EventHandler
    public void onBlockBreak(BlockBreakEvent e)
    {
    	e.setCancelled(true);
    }
    @EventHandler
    public void onBlockPlace(BlockPlaceEvent e)
    {
    	e.setCancelled(true);
    }
    @EventHandler
    public void onFoodChange(FoodLevelChangeEvent event)
    {
    	event.setCancelled(true);
    }
}
