package org.mcsg.oitc;

import net.minecraft.server.v1_6_R2.Packet205ClientCommand;

import org.bukkit.Bukkit;
import org.bukkit.command.ConsoleCommandSender;
import org.bukkit.craftbukkit.v1_6_R2.entity.CraftPlayer;
import org.bukkit.entity.Player;
import org.bukkit.plugin.java.JavaPlugin;
import org.mcsg.api.API;
import org.mcsg.oitc.Commands.ForcestartCommand;
import org.mcsg.oitc.Commands.LeaveCommand;
import org.mcsg.oitc.Commands.ModeCommand;
import org.mcsg.oitc.Commands.VoteCommand;
import org.mcsg.oitc.ConfigManager.Config;
import org.mcsg.oitc.ConfigManager.MapConfig;
import org.mcsg.oitc.GameEngine.Engine;
import org.mcsg.oitc.ServerInteraction.InteractType;
import org.mcsg.oitc.ServerInteraction.ServerInteractor;

public class OITC extends JavaPlugin
{
	private static OITC instance;
	private static Lilypad lilyInstance;
	private static String serverName;
	private static ConsoleCommandSender consoleCommandSender;
	private static boolean acceptingPlayers = false;
	
	private MapConfig mapConfig;
	private Config config;
	
	@Override
	public void onEnable() {
		instance = this;
		consoleCommandSender = getServer().getConsoleSender();
		lilyInstance = new Lilypad();
		Events.setup();
		API.setDebugEnabled(true);
		setServerName(getServer().getServerName());
		WorldHandler.initWorlds(true);
	}
	public void finishEnable() {
		WorldHandler.loadWorlds();
		Engine.Prep();
		
		getCommand("fs").setExecutor(new ForcestartCommand());
		getCommand("leave").setExecutor(new LeaveCommand());
		getCommand("mode").setExecutor(new ModeCommand());
		getCommand("vote").setExecutor(new VoteCommand());
		setAcceptingPlayers(true);
		
		ServerInteractor.notifyServer(InteractType.UPDATE);
		
		//Just to double check the server is aware that we work
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(this, new Runnable()
		{
			public void run() {
				ServerInteractor.notifyServer(InteractType.UPDATE);
			}
		}, 60L);
	}
	@Override
	public void onDisable() {
		ServerInteractor.notifyServer(InteractType.SHUTDOWN);
		
		for(Player p : Bukkit.getOnlinePlayers()) {
			getLilypad().sendToHub(p);
		}
		WorldHandler.deleteWorlds();
	}
	
	public static void scheduleKick(final Player p, final String reason) {
		Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(OITC.getPlugin(), new Runnable()
		{
			public void run() {
				if (p != null) {
					if (p.isOnline()) {
						p.kickPlayer(reason);
					}
				}
			}
		}, 20L);
	}
	
	public static void instaRespawn(Player p)
    {
        Packet205ClientCommand packet = new Packet205ClientCommand();
        packet.a = 1;
        ((CraftPlayer) p).getHandle().playerConnection.a(packet);
 
    }
	
	//Get & Set Accessors
	public static String getSeverName() {
		return serverName;
	}
	public static void setServerName(String str) {
		serverName = str;
	}
	public static boolean getAcceptingPlayers() {
		return acceptingPlayers;
	}
	public static void setAcceptingPlayers(boolean accepting) {
		acceptingPlayers = accepting;
	}
	public static Lilypad getLilypad() {
		return lilyInstance;
	}
	public static OITC getPlugin() {
		return instance;
	}
	public MapConfig getMapConfig() {
		return mapConfig;
	}
	public void setMapConfig(MapConfig config) {
		mapConfig = config;
	}
	public Config getConfig_() {
		return config;
	}
	public void setConfig(Config config) {
		this.config = config;
	}
	public static ConsoleCommandSender getConsoleCommandSender() {
		return consoleCommandSender;
	}
}
