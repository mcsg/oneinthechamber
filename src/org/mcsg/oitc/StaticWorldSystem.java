package org.mcsg.oitc;

import java.io.File;
import java.io.FileNotFoundException;

import org.bukkit.ChatColor;
import org.bukkit.Difficulty;
import org.bukkit.WorldCreator;

public class StaticWorldSystem
{
	String world = "";
	public StaticWorldSystem()
	{
		
	}
	public void LoadWorld(String name) {
		WorldCreator wc = new WorldCreator(name);
		wc.generator(new VoidChunkGenerator());
		OITC.getPlugin().getServer().createWorld(wc);
		OITC.getPlugin().getServer().getWorld(wc.name()).setAutoSave(false);
		OITC.getPlugin().getServer().getWorld(wc.name()).setKeepSpawnInMemory(true);
		OITC.getPlugin().getServer().getWorld(wc.name()).setWeatherDuration(0);
		OITC.getPlugin().getServer().getWorld(wc.name()).setDifficulty(Difficulty.PEACEFUL);
		world = wc.name();
		
		OITC.getPlugin().getServer().getConsoleSender().sendMessage(ChatColor.GREEN + "Loaded world: "+ChatColor.GOLD+world);
	}
	public void UnloadWorld() {
		OITC.getPlugin().getServer().unloadWorld(world, false);//false = Don't save
	}
	public void DeleteWorld() {
		if (OITC.getPlugin().getServer().getWorld(world) != null) {
			UnloadWorld();
			String absopath = OITC.getPlugin().getServer().getWorldContainer().getAbsolutePath();
			String path = (absopath.substring(0, absopath.length()-1) + world);
			try {
				deleteRecursive(new File(path));
			} catch (FileNotFoundException e) {
				e.printStackTrace();
			}
		}
	}
	
	public boolean deleteRecursive(File path) throws FileNotFoundException{
        if (!path.exists()) throw new FileNotFoundException(path.getAbsolutePath());
        boolean ret = true;
        if (path.isDirectory()){
            for (File f : path.listFiles()){
                ret = ret && deleteRecursive(f);
            }
        }
        return ret && path.delete();
    }
}
