package org.mcsg.oitc.ServerInteraction;

public class SignUpdatePacket
{
	public byte id;
	public int length;
	public String server;
	public String line1;
	public String line2;
	public String line3;
	public String line4;
	
	public SignUpdatePacket() {}
	public SignUpdatePacket(byte[] data) {
		Packet pkt = new Packet(data);
		id = pkt.ReadByte();
		length = pkt.ReadByte();
		
		server = pkt.ReadString(pkt.ReadByte());
		
		line1 =pkt.ReadString(pkt.ReadByte());
		line2 =pkt.ReadString(pkt.ReadByte());
		line3 =pkt.ReadString(pkt.ReadByte());
		line4 =pkt.ReadString(pkt.ReadByte());
	}
	
	public byte[] Build()
	{
		Packet pkt = new Packet();
		pkt.WriteByte(id);//ID
		pkt.WriteByte((byte)0);//Length placeholder, applied @ toArray
		pkt.WriteString(server);
		pkt.WriteString(line1);
		pkt.WriteString(line2);
		pkt.WriteString(line3);
		pkt.WriteString(line4);
		
		return pkt.toArray();
	}
}
