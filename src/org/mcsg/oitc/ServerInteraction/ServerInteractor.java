package org.mcsg.oitc.ServerInteraction;

import lilypad.client.connect.api.request.impl.MessageRequest;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.mcsg.api.API;
import org.mcsg.oitc.Lilypad;
import org.mcsg.oitc.OITC;
import org.mcsg.oitc.GameEngine.GameVariables;

public class ServerInteractor
{
	//Amount of lines on a sign, currently 4
	static int signLineAmount = 4;
	private static boolean announcedShutdown = false;
	
	public static void notifyServer(InteractType type) {
		API.debug("Notifying server of type: "+type);
		switch(type)
		{
			case UPDATE:
				standardUpdate();
				break;
			case SHUTDOWN:
				shutdownUpdate();
				break;
		}
	}
	static void standardUpdate() {
		if (announcedShutdown) { return; }
		String[] lines = new String[getLineAmount()];
		switch(GameVariables.getGame().getStatus()) {
			case WAITING:
				lines[0] = ChatColor.GREEN + "[JOIN]";
				break;
			case INGAME:
				lines[0] = ChatColor.RED + "[INGAME]";
				break;
		}
		if (GameVariables.getGame().Full())
			lines[0] = ChatColor.RED + "[FULL]";
		
		lines[1] = ChatColor.YELLOW+GameVariables.getMap().getName();
		lines[2] = ChatColor.DARK_PURPLE+OITC.getSeverName().toUpperCase();
		lines[3] = ChatColor.BLUE+""+GameVariables.getGame().getPlayerCount()+"/"+GameVariables.getMaxPlayers();
		
		//Static class in DreadAPI
		//TODO: Send to server
		sendMessage(constructPacket(lines));
		
	}
	static void shutdownUpdate() {
		announcedShutdown = true;
		
		String[] lines = new String[getLineAmount()];
		lines[0] = ChatColor.RED+""+ChatColor.BOLD+"RESTARTING";
		lines[1] = ChatColor.GREEN + OITC.getSeverName().toUpperCase();
		lines[2] = "";
		lines[3] = lines[0];
		
		//Static class in DreadAPI
		//TODO: Send to server
		sendMessage(constructPacket(lines));
	}
	static byte[] constructPacket(String[] data) {
		SignUpdatePacket pkt = new SignUpdatePacket();
		pkt.id = 0x01;
		pkt.line1 = data[0];
		pkt.line2 = data[1];
		pkt.line3 = data[2];
		pkt.line4 = data[3];
		pkt.server = OITC.getSeverName();
		
		return pkt.Build();
	}
	
	static int getLineAmount() {
		return signLineAmount;
	}
	
	public static void sendMessage(final byte[] pkt)
	{
		API.debug("Attemping to send packet(of length: "+pkt.length+") to lilypad 'oitcsignserver' channel");
		if (Lilypad.getConnect().isConnected()) {
		    MessageRequest request = null;
		    try
		    {
		        request = new MessageRequest(Lilypad.getHubServer(), "oitcsignserver", pkt);
				Lilypad.getConnect().request(request);
		    }
		    catch (Exception e){e.printStackTrace();}
		}
		else {
			Bukkit.getServer().getScheduler().scheduleSyncDelayedTask(OITC.getPlugin(), new Runnable()
			{
				public void run() {
					if (announcedShutdown) {
						notifyServer(InteractType.SHUTDOWN);
					}
					else {
						notifyServer(InteractType.UPDATE);
					}
				}
			}, 20L);
		}
	}
}
