package org.mcsg.oitc.ServerInteraction;

import java.util.ArrayList;

public class Packet
{
	ArrayList<Byte> Buffer;
	int offset = 0;
	byte[] readArray;
	
	public Packet() {
		Buffer = new ArrayList<Byte>();
	}
	public Packet(byte[] data) {
		readArray = data;
	}
	public void WriteByte(byte value) {
		Buffer.add(value);
	}
	public Byte ReadByte() {
		byte rtn = readArray[offset];
		offset+=1;
		return rtn;
	}
	public String ReadString(int len) {
		byte[] bytes = new byte[len];
		for (int i=0;i<len;i++) {
			bytes[i] = readArray[offset];
			offset+=1;
		}
		return new String(bytes);
	}
	public void WriteString(String str) {
		byte[] bytes = str.getBytes();
		WriteByte((byte)bytes.length);
		for (int i =0;i<bytes.length;i++) {
			WriteByte(bytes[i]);
		}
	}
	public byte[] toArray() {
		Object[] array = Buffer.toArray();
		byte[] data = new byte[array.length];
		for (int i =0;i<array.length;i++) {
			data[i] = (byte)array[i];
		}
		data[1] = (byte)array.length;
		return data;
	}
} 
