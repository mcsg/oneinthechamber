package org.mcsg.oitc.ConfigManager;

import org.bukkit.configuration.file.FileConfiguration;
import org.mcsg.oitc.Lilypad;
import org.mcsg.oitc.OITC;
import org.mcsg.oitc.GameEngine.GameVariables;
import org.mcsg.oitc.GameEngine.GameVariables.GameMode;

public class Config
{
	FileConfiguration config;
	public Config() { Load(); }
	
	public void Load()
	{
		config = OITC.getPlugin().getConfig();
		if (!config.contains("hub-server"))
			config.set("hub-server", "gungame");
		else
			Lilypad.setHubServer(config.getString("hub-server"));
		
		if (!config.contains("max-players"))
			config.set("max=players", GameVariables.getMaxPlayers());
		else
			GameVariables.setMaxPlayers(config.getInt("max-players"));
		
		if (!config.contains("min-players"))
			config.set("min-players", GameVariables.getMinPlayers());
		else
			GameVariables.setMinPlayers(config.getInt("min-players"));
		
		if (!config.contains("lobby-time"))
			config.set("lobby-time", GameVariables.getLobbyTime());
		else
			GameVariables.setLobbyTime(config.getInt("lobby-time"));
		
		if (!config.contains("lobby-shorttime"))
			config.set("lobby-shorttime", GameVariables.getLobbyShortTime());
		else
			GameVariables.setLobbyShortTime(config.getInt("lobby-shorttime"));
		
		if (!config.contains("game-time"))
			config.set("game-time", GameVariables.getGameTime());
		else
			GameVariables.setGameTime(config.getInt("game-time"));
		
		if (!config.contains("game-mode"))
			config.set("game-mode", "ELIMINATION");
		else
			GameVariables.gameMode = GameMode.valueOf(config.get("game-mode").toString().toUpperCase());
		
		if (!config.contains("voting"))
			config.set("voting", true);
		else
			GameVariables.setVoting(config.getBoolean("voting"));
		//TODO: Config based game-mode
		
		switch(GameVariables.getGameMode())
		{
			case HARDCORE:
			case ELIMINATION:
			{
				if (!config.contains("max-lifes"))
					config.set("max-lifes", GameVariables.getLifes());
				else
					GameVariables.setLifes(config.getInt("max-lifes"));
				break;
			}
			case KILLS: { break; }
		}
		OITC.getPlugin().saveConfig();
	}
}
